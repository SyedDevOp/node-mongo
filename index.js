const mongoose = require('mongoose');
const Dishes = require('./models/dishes');
const url = 'mongodb://localhost:27017/conFusion';  //specifying localtion of the database
const connect = mongoose.connect(url);
connect.then((db)=>{
    console.log('Connected to Server');

    Dishes.create({
        name: 'Uthappizza',
        description: 'test'
    })
    .then((dish)=>{
        console.log(dish);
        return Dishes.findByIdAndUpdate(dish._id,{
            $set: {discription: 'Updated Test'}
        },{
            new: true                               //--> updated dish print karo
        }).exec();
    })
    .then((dish)=>{
        console.log(dish);

        dish.comment.push({                 // adding a comment is dish document
            rating: 5,
            comment: 'Lovely',
            author: 'SyedDevOps'
        });
        return dish.save();
    })
    .then((dish)=>{
        console.log(dish);
        return Dishes.remove({});
    })
    .then(()=>{
        return mongoose.connection.close();
    })
    .catch((err)=>{
        console.log()
    }); 
});