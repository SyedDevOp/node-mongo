const assert = require('assert');

exports.insertDocument = (db, document, collection, callback) => {
    const coll = db.collection(collection);       //coll main hm database (parameter say aya hoa) say collection utha rahe hain collection var(peramter say naam aya ha)
    coll.insert(document, (err, result)=>{
        assert.equal(err, null);
        console.log('Inserted' + result.result.n + ' document into collection' + collection);
        callback(result);
    });
};
exports.findDocuments = (db, collection, callback)=>{
    const coll = db.collection(collection);
    coll.find({}).toArray((err, docs)=>{
        assert.equal(err, null);
        callback(docs)
    });
};
exports.removeDocument = (db, document, collection, callback)=>{
    const coll = db.collection(collection);
    coll.deleteOne(document, (err, result)=>{
        assert.equal(err, null);
        console.log("Removed the document", document);
        callback(result);
    }); 
};
exports.updateDocument = (db, document, update, collection, callback)=>{
    const coll = db.collection(collection);
    coll.updateOne(document, {$set: update}, null, (err, result)=>{
        assert.equal(err, null);
        console.log('Updated the Document:', update);
        callback(result);
    });
};