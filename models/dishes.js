const mongoose = require('mongoose');
const schema = mongoose.Schema;

const commentSchema = new schema({
    rating:{
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment:{
        type: String,
        required: true
    },
    author:{
        type: String,
        required: true
    } 
}, {timestamps: true
});

const dishSchema = new schema({     //creating a new schema for dishes
    name: {                         //specifying first type as name
        type: String,               //specifying its properties like name should be a string
        required: true,             //cannot be empty
        unique: true                //must be unique
    },
    description:{
        type: String,
        required: true,
    },
    comment: [commentSchema]        //subdocument
},{
    timestamps: true                //mongoose will automatically add it
    });

    var Dishes = mongoose.model('Dish', dishSchema);
    module.exports = Dishes;